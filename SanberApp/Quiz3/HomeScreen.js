import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) { 
    let tambahPrice = parseInt(price)
    let total = this.state.totalPrice + tambahPrice
    this.setState({totalPrice: total})
  }


  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,
           <Text>
             {this.props.route.params.userName}{'\n'} 
            </Text>
            </Text>
              <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        <FlatList
        data={data.produk}
        numColumns={2}
        keyExtractor={(data) => data.id}
        renderItem={(item) => <ListItem data={item.item}
        onPress={() => this.updatePrice(item.item.harga)}/>}
        />
      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue' onPress={this.props.onPress} />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  itemContainer: {
    width: DEVICE.width * 0.44,
    margin:10,
    padding:10,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: '#FFF'
  },
  itemImage: {
    flex:1,
    width:100,
    height:100
  },
  itemName: {
    fontWeight:'bold',
    marginTop: 5,
    marginBottom:10
  },
  itemPrice: {
    color:'deepskyblue',
    marginBottom: 5
  },
  itemStock: {
    marginBottom:5
  },
  itemButton: {
    marginBottom:5
  },
  buttonText: {
    alignSelf:'center'
  }
})
