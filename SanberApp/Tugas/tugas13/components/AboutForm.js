import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TouchableOpacity,
	Image
} from 'react-native';
import {
	Item,
	Input,
	Form,
	Label,
	Button,
	Thumbnail,
	Text
} from 'native-base';



import Logo from '../images/logo.png';

class AboutForm extends Component{
	constructor(props)
	{
			super(props);
	}

	render() {
		return (
			<View style={styles.containerStyle}>
				<View style={styles.logoStyle}>
					<Image source={Logo}/>
				</View>	
				<Form style={styles.formLoginStyle}>
					<Item floatingLabel>
						<Label>
							<Text style={styles.inputStyle}>Username</Text>
						</Label>
						<Input style={styles.inputStyle}/>
					</Item>
					<Item floatingLabel>
						<Label>
							<Text style={styles.inputStyle}>Password</Text>
						</Label>
						<Input style={styles.inputStyle}/>
					</Item>
				</Form>
				<Button style={styles.footerBottomStyle}>
					
				</Button>	
			</View>
		)
	}
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	bgImageStyle: {
		flex: 1,
		resizeMode: 'cover',
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	logoStyle:
	{
	 	marginTop: 70,
	 	marginBottom: 80,
	 	alignItems: 'center',
	 	justifyContent: 'center'
	},
	textLogoStyle: {
		fontSize: 15,
		color: 'white'
	},
	formLoginStyle: {
		marginTop: -30,
		paddingLeft: 10,
		paddingRight: 30
	},
	inputStyle: {
		color: 'black',
		marginBottom: 6,
		fontSize: 14
	}
})

export default AboutForm;