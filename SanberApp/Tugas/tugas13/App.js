import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import LoginForm from './components/LoginForm';
import AboutForm from './components/AboutForm';

export default class App extends Component{
	render() {
		return (
			<LoginForm/>
			<AboutForm/>
		); 
	}
}