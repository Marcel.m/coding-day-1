import React from 'react';
import {StyleSheet, Text, View, StatusBar, ScrollView} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const AboutScreen = () => {
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <ScrollView showsVerticalScrollIndicator={false}>
        <RenderHeader />
        <RenderContent />
      </ScrollView>
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor="#FFFFFF"
      hidden={false}
    />
  );
};

const RenderHeader = () => {
  return (
    <View style={styles.header}>
      <Text style={styles.titleHeader}>About</Text>
    </View>
  );
};

const RenderContent = () => {
  return (
    <View style={styles.content}>
      <RenderProgrammingLang />
      <RenderFramework />
      <RenderTeknologi />
    </View>
  );
};

const RenderProgrammingLang = () => {
  return (
    <View>
      <Text style={styles.label}>Programming Language</Text>
      <View style={styles.skills}>
        <MaterialCommunityIcons
          name="language-javascript"
          size={28}
          color="#f7df1e"
        />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Intermediate</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue} />
            <View style={styles.percentaseGrey} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>63%</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.skills}>
        <MaterialCommunityIcons
          name="language-typescript"
          size={28}
          color="#007acc"
        />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Advanced</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue} />
            <View style={styles.percentaseGrey} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>90%</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const RenderFramework = () => {
  return (
    <View style={styles.wrapperLabel}>
      <Text style={styles.label}>Framework Library</Text>
      <View style={styles.skills}>
        <MaterialCommunityIcons name="react" size={28} color="#00d9ff" />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Begginer</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue} />
            <View style={styles.percentaseGrey} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>36%</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.skills}>
        <MaterialCommunityIcons name="bootstrap" size={28} color="#533b78" />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Intermediate</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue} />
            <View style={styles.percentaseGrey} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>70%</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const RenderTeknologi = () => {
  return (
    <View style={styles.wrapperLabel}>
      <Text style={styles.label}>Teknologi</Text>
      <View style={styles.skills}>
        <MaterialCommunityIcons name="firebase" size={28} color="#f7c52c" />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Intermediate</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue60} />
            <View style={styles.percentaseGrey60} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>60%</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.skills}>
        <MaterialCommunityIcons
          name="github-circle"
          size={28}
          color="#302f2f"
        />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Advanced</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue80} />
            <View style={styles.percentaseGrey80} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>93%</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.skills}>
        <MaterialCommunityIcons name="git" size={28} color="#e94e31" />
        <View style={styles.percentaseWrapper}>
          <Text style={styles.title}>Intermediate</Text>
          <View style={styles.percentase}>
            <View style={styles.percentaseBlue80} />
            <View style={styles.percentaseGrey80} />
            <View style={styles.wrapperTitlePercentase}>
              <Text style={styles.title}>80%</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleHeader: {
    flex: 1,
    fontSize: 20,
    fontWeight: '800',
    textAlign: 'center',
  },
  content: {
    paddingVertical: 40,
  },
  wrapperLabel: {
    paddingVertical: 20,
  },
  label: {
    fontSize: 16,
    color: '#7D8797',
    marginBottom: 6,
  },
  skills: {
    flexDirection: 'row',
    paddingTop: 10,
    alignItems: 'center',
  },
  title: {
    fontSize: 14,
    color: '#7D8797',
  },
  percentaseWrapper: {
    paddingLeft: 10,
  },
  percentase: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  percentaseBlue: {
    backgroundColor: '#0BCAD4',
    width: 140,
    height: 2,
  },
  percentaseGrey: {
    backgroundColor: '#7D8797',
    width: 40,
    height: 2,
  },
  percentaseBlue80: {
    backgroundColor: '#0BCAD4',
    width: 150,
    height: 2,
  },
  percentaseGrey80: {
    backgroundColor: '#7D8797',
    width: 30,
    height: 2,
  },
  percentaseBlue60: {
    backgroundColor: '#0BCAD4',
    width: 100,
    height: 2,
  },
  percentaseGrey60: {
    backgroundColor: '#7D8797',
    width: 80,
    height: 2,
  },
  wrapperTitlePercentase: {
    paddingLeft: 10,
  },
});


