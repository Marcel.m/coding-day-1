import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ScrollView,
} from 'react-native';

const LoginScreen = ({navigation}) => {
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderHeader />
      <RenderContent navigation={navigation} />
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor="#FFFFFF"
      hidden={false}
    />
  );
};

const RenderHeader = () => {
  return (
    <View style={styles.header}>
      <Text style={styles.titleHeader}>Daftar Akun</Text>
      <Image source={require('./images/logo.png')} style={styles.img} />
    </View>
  );
};

const RenderContent = ({navigation}) => {
  return (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Email</Text>
        <TextInput placeholder="marcel.13sdh@gmail.com" style={styles.input} />
      </View>
      <View style={styles.wrapperInput}>
        <Text style={styles.label}>Password</Text>
        <TextInput
          placeholder="********"
          style={styles.input}
          secureTextEntry={true}
        />
      </View>
      <View style={styles.gap} />
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('MyDrawer')}>
        <Text style={styles.titleButton}>Continue</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 40,
    paddingVertical: 20,
    justifyContent: 'space-around',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    marginVertical: 40,
    width: 250,
    height: 75,
  },
  content: {
    paddingTop: 40,
  },
  titleHeader: {
    fontSize: 20,
    fontWeight: '800',
  },
  wrapperInput: {
    paddingBottom: 10,
  },
  label: {
    fontSize: 16,
    color: '#7D8797',
    marginBottom: 6,
    fontFamily: '400',
  },
  input: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#E9E9E9',
    padding: 12,
    color: '#112340',
  },
  gap: {
    height: 40,
  },
  button: {
    paddingVertical: 15,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0BCAD4',
  },
  titleButton: {
    fontSize: 18,
    color: '#FFFFFF',
    fontWeight: '600',
  },
});


