var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var time=10000

function baca(x){
	readBooksPromise(time,books[x])
	.then(function(fulfilled){
		time=fulfilled
		baca(x+1)
	})
	.catch(function(rejected){
	})
}

baca(0)
