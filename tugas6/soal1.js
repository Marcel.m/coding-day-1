
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

function arrayToObject(arr) {
    var now = new Date()
	var thisYear = now.getFullYear()
   for (var i = 0; i < arr.length; i++){
   	var tahun = arr[i][3]
   	var personObj = {
    firstName : arr[i][0],
    lastName: arr[i][1],
    gender: arr[i][2],
 	age:""
	}

   if (tahun>thisYear || tahun==null){personObj.age="Invalid birth year"}

	else {personObj.age = thisYear - tahun}

	console.log(i + 1 + "." + "firstName: " + personObj.firstName)
	console.log("lastName: " + personObj.lastName)
	console.log("gender: " + personObj.gender)
	console.log("age: " + personObj.age)
	console.log("")
   }
 }

/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
