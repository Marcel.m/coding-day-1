class Animal {
constructor(name){
this._name = name
this._legs = 4
this._cold_blooded = false
}
get name() {
	return this._name
}
set name(x) {
	this._name=x
}
get legs() {
	return this._legs
}
set legs(x) {
	this._legs=x
}
get cold_blooded() {
	return this._cold_blooded
}
set cold_blooded(x) {
	this._cold_blooded=x
}


}


class Ape extends Animal {
  constructor(name) {
    super(name);
    this._name = name;
    this._legs = 2;
    this._cold_blooded = false;
  }
  yell() {
    return "Auooo"; 
  }
  legs() {
  	return this._legs;
  }
cold_blooded(){
	return this._cold_blooded;
}

}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
console.log(sungokong.legs())
console.log(sungokong.cold_blooded())


class Frog extends Animal {
  constructor(name) {
  	super(name);
     this._name = name;
     this._legs = 4;
     this._cold_blooded = true;

  }
  jump() {
    return "hop hop";
  }
legs(){
	return this._legs
}
cold_blooded(){
	return this._cold_blooded
}
}
  

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 
console.log(kodok.legs())
console.log(kodok.cold_blooded())


