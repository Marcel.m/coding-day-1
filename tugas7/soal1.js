class Animal {
constructor(name){
this._name = name
this._legs = 4
this._cold_blooded = false
}
get name() {
	return this._name
}
set name(x) {
	this._name=x
}
get legs() {
	return this._legs
}
set legs(x) {
	this._legs=x
}
get cold_blooded() {
	return this._cold_blooded
}
set cold_blooded(x) {
	this._cold_blooded=x
}


}


var sheep = new Animal("shaun");

 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
 

